package com.example.scpcatalog

import com.example.scpcatalog.interactors.SCPCatalogInteractor
import com.example.scpcatalog.presenters.CatalogPresenter

object LameArsedDI {

    fun getCatalogPresenter(): CatalogPresenter = CatalogPresenter(SCPCatalogInteractor(SCPRepository()))

}