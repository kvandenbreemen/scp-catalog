package com.example.scpcatalog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.scpcatalog.model.CatalogEntry
import com.example.scpcatalog.presenters.CatalogPresenter
import com.example.scpcatalog.viewmodels.OverviewViewModel
import com.example.scpcatalog.views.CatalogEntryFragment
import com.example.scpcatalog.views.CatalogListFragment

class MainActivity : AppCompatActivity() {

    private var presenter: CatalogPresenter? = null

    private var observer = Observer<CatalogEntry> {entry->
        supportFragmentManager.beginTransaction().replace(R.id.main_content, CatalogEntryFragment(entry)).commit()
    }

    private val overviewViewModel: OverviewViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = LameArsedDI.getCatalogPresenter().also {
            val listFragment = CatalogListFragment(it)
            supportFragmentManager.beginTransaction().replace(R.id.side_panel, listFragment).commit()
        }

        overviewViewModel.selectedLiveData.observe(this, observer)

    }

    override fun onResume() {
        super.onResume()

        presenter?.update()
    }



    override fun onDestroy() {
        super.onDestroy()
        overviewViewModel.run {
            this.selectedLiveData.removeObserver(observer)
        }
        presenter = null
    }
}