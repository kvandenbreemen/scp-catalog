package com.example.scpcatalog

import com.example.scpcatalog.model.CatalogEntry

class SCPRepository() {

    suspend fun getSCPs(): List<CatalogEntry> {
        return listOf(
            CatalogEntry(2004, "SCP-2004 is contained at Armed Reliquary Containment Area-02. Standard memetic countermeasures have proven insufficient in the past; therefore, SCP-2004 is to be handled utilizing Containment Procedure-2004 \"Blind Lead the Blind.\" See ARC A-02 Clearance-04 Procedures manual for more information. Any individuals affected by SCP-2004 (hereby dubbed SCP-2004-1) are to be handled in the same manner.",
            "SCP-2004 is a set of five hand-held personal data assistants of unknown, possibly extraterrestrial origin. Since acquisition, all but one have become inert and no longer function. SCP-2004 is composed of an unknown material whose molecular structure matches nothing on the Foundation's expanded periodic table of elements, flexible like plastic yet resistant to extreme temperatures and physical damage. Each device is transparent green with smooth edges, with no apparent power source or input/output ports. SCP-2004 activates when it makes physical contact with an active bioelectric field, projecting a three-dimensional holographic document.",
                imageUrl = "https://scp-wiki.wdfiles.com/local--files/scp-6061/bloodycarpet.jpg"
                ),
            CatalogEntry(2199, "SCP-2199 is in a standard Wildlife Observation Chamber, located within Site-77. Listening devices are attached to the hump on a permanent basis, in order to observe the interior of its body. Exploratory testing has been suspended, due to the effect that repeated testing had on SCP-2199's health.",
                "SCP-2199 is an adult male Indian Camel (Camelus dromedarius). The hump is hairless, and has the appearance of being covered in scar tissue.\n" +
                        "\n" +
                        "Instead of being completely composed out of tissue fibers, the hump on SCP-2199's back is filled with water underneath a 10cm layer of fibrous tissue. This water is brackish, self-replenishing, and inhabits a volume roughly equivalent to the total biomass of SCP-21991. Despite the absence of any corresponding light sources, it contains ambient light.\n" +
                        "\n" +
                        "A large number of small organisms, collectively designated as SCP-2199-1, inhabit this space. Each subgroup of SCP-2199-1 resembles a particular species of plant or animal native to an arid region, but has adapted or been heavily modified for small size and aquatic environments; many display extremely unusual body plans. While still superficially resembling their desert counterparts, all SCP-2199-1 instances are between 2 and 10 cm long and the relative sizes of SCP-2199-1 subgroups do not correspond to the relative sizes of their unmodified relatives. Since direct examination of this ecosystem requires that SCP-2199 be anesthetized and its hump surgically opened, visual observation has been limited.\n" +
                        "\n" +
                        "It is, however, possible to observe SCP-2199-1 from outside the hump by listening to their vocalizations and other audio traces. Regular monitoring of SCP-2199-1 is carried out almost entirely via audio, in order to minimize damage to SCP-2199 due to invasive testing.",
                imageUrl = "https://scp-wiki.wdfiles.com/local--files/scp-5054/p.jpg"
                )
        )
    }

}