package com.example.scpcatalog.interactors

import com.example.scpcatalog.SCPRepository
import com.example.scpcatalog.model.CatalogEntry

class SCPCatalogInteractor(private val scpRepository: SCPRepository) {

    suspend fun getCatalog(): List<CatalogEntry> = scpRepository.getSCPs()

}