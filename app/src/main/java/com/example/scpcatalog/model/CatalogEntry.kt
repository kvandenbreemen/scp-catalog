package com.example.scpcatalog.model

data class CatalogEntry(

    /**
     * For example, SCP-2004 would have ID 2004
     */
    val id: Int,
    val specialContainmentProcedures: String,
    val description: String,
    val imageUrl: String
)