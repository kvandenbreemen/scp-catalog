package com.example.scpcatalog.presenters

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.scpcatalog.interactors.SCPCatalogInteractor
import com.example.scpcatalog.model.CatalogEntry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CatalogPresenter(private val catalogInteractor: SCPCatalogInteractor) {

    private val mutableLiveData = MutableLiveData<List<CatalogEntry>>()
    val liveData: LiveData<List<CatalogEntry>> get() =  mutableLiveData

    fun update() {
        CoroutineScope(Dispatchers.IO).launch {
            mutableLiveData.postValue(catalogInteractor.getCatalog())
        }
    }

}