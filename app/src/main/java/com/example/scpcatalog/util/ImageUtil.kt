package com.example.scpcatalog.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

object ImageUtil {

    @JvmStatic
    fun useGlideToFetchImage(imageView: ImageView, url: String) {
        Glide.with(imageView.context).load(url).into(imageView)
    }

    @BindingAdapter("android:myImageUrl")
    @JvmStatic
    fun loadImage(imageView: ImageView, url: String) {
        useGlideToFetchImage(imageView, url)
    }

}