package com.example.scpcatalog.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.scpcatalog.model.CatalogEntry

class OverviewViewModel(): ViewModel() {

    private val selectedItem = MutableLiveData<CatalogEntry>()
    val selectedLiveData: LiveData<CatalogEntry> get() = selectedItem

    private val list = MutableLiveData<List<CatalogEntry>>()


    var catalogList: List<CatalogEntry>? = null
    set(value) {
        value?.let { values->

        }
    }

    var selected: CatalogEntry? = null
    set(value){
        value?.let { entry ->
            selectedItem.postValue(entry)
        }
    }

}