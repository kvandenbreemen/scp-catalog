package com.example.scpcatalog.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.scpcatalog.databinding.FrgScpDetailBinding
import androidx.fragment.app.Fragment
import com.example.scpcatalog.R
import com.example.scpcatalog.model.CatalogEntry

class CatalogEntryFragment(private val catalogEntry: CatalogEntry): Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val dataBinding: FrgScpDetailBinding = DataBindingUtil.inflate(
            inflater, R.layout.frg_scp_detail, container, false
        )

        dataBinding.entry = catalogEntry

        return dataBinding.root
    }

}