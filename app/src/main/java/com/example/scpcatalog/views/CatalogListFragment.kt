package com.example.scpcatalog.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.scpcatalog.R
import com.example.scpcatalog.databinding.LyoScpItemBinding
import com.example.scpcatalog.model.CatalogEntry
import com.example.scpcatalog.presenters.CatalogPresenter
import com.example.scpcatalog.viewmodels.OverviewViewModel

class CatalogListFragment(private val catalogPresenter: CatalogPresenter): Fragment() {

    private val observer = Observer<List<CatalogEntry>> {
        adapter?.apply {
            dataSet = it
            notifyDataSetChanged()
        }
    }

    private val viewModel: OverviewViewModel by activityViewModels()

    private class ItemViewHolder(val binding: LyoScpItemBinding): RecyclerView.ViewHolder(binding.root) {

    }

    private class ItemAdapter(var dataSet: List<CatalogEntry>, private val viewModel: OverviewViewModel): RecyclerView.Adapter<ItemViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
            return ItemViewHolder(DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.lyo_scp_item, parent, false
            ))
        }

        override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
            holder.binding.entry = dataSet[position]
            holder.binding.listener = object: SCPClickListener {
                override fun onItemClicked(view: View) {
                    viewModel.selected = dataSet[position]
                }

            }
            holder.itemView.setOnClickListener { _->
                Toast.makeText(
                    holder.itemView.context,
                    "Need to click the button",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        override fun getItemCount(): Int {
            return dataSet.size
        }

    }

    private var adapter: ItemAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ItemAdapter(listOf(), this.viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frg_scp_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        view.findViewById<RecyclerView>(R.id.scps).apply {

            layoutManager = LinearLayoutManager(context)
            adapter = this@CatalogListFragment.adapter

        }

        catalogPresenter.liveData.observe(viewLifecycleOwner, observer)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {

        catalogPresenter.liveData.removeObserver(observer)

        super.onDestroyView()
    }

    override fun onDestroy() {
        adapter = null
        super.onDestroy()
    }

}