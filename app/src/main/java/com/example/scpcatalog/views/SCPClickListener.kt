package com.example.scpcatalog.views

import android.view.View

interface SCPClickListener {

    fun onItemClicked(view: View)

}